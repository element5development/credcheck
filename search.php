<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<header class="page-title has-image" style="background-image: url('/wp-content/uploads/2018/05/CC-Header-Blog.jpg');">
	<div class="block">
		<?php if (!have_posts()) : ?>
		<h1 class="has-subheader">Nothing Found</h1>
			<p class="subheader">Our background checks are very thorough. But even we couldn't find a page for: <?php echo get_search_query(); ?></p>
		<?php else : ?>
			<h1 class="has-subheader">Search Results</h1>
			<p class="subheader">Our background checks on "<?php echo get_search_query(); ?>" lead to some results.</p>
		<?php endif; ?>
	</div>
	<!-- BACKGROUND OVERLAY -->
	<div class="overlay"></div>
	<!-- /BACKGROUND OVERLAY -->
	<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pbsa-accredited.png" alt="pbsa accredited" />
	<svg id="golden-triangle" height="443" viewBox="0 0 223 443" width="223" xmlns="http://www.w3.org/2000/svg">
		<linearGradient id="a" x1="7.7911%" x2="50%" y1="15.2016%" y2="100%">
			<stop offset="0" stop-color="#aa8241"/>
			<stop offset="1" stop-color="#c6ae6e"/>
		</linearGradient>
		<g fill="none" fill-rule="evenodd" stroke="url(#a)" stroke-width="6" transform="rotate(-90 219 465)">
			<path d="M68 .0013L461.4987 395 855 0"/>
			<path d="M45 0l416.002 418L877 .0053"/>
			<path d="M22 0l439.5934 441L901 .1873"/>
			<path d="M0 .054L462.4724 464 925 0"/>
		</g>
	</svg>
</header>

<main>
	<a id="content" class="anchor"></a>
	<section class="feed-search default-contents	">
		<?php while (have_posts()) : the_post(); ?>
			<artcile class="preview-search">
				<a href="<?php the_permalink(); ?>">
					<h1><?php the_title(); ?></h1>
					<p><?php echo get_the_excerpt(); ?></p>
				</a>
			</artcile>
		<?php endwhile; ?>
	</section>

	<section class="infinite-scroll">
		<?php the_posts_pagination( array(
			'prev_text'	=> __( 'Previous page' ),
			'next_text'	=> __( 'Next page' ),
		) ); ?>
		<a class="button is-primary load-more">View more</a>
	</section>
</main>

<?php get_template_part('template-parts/footers/footer'); ?>

<?php get_footer(); ?>