<?php

/*-----------------------------------------
  INCLUDE CSS AND JS
-----------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
	wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*-----------------------------------------
  Enable HTML5 Markup Support
-----------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*-----------------------------------------
  Enable Excerpts
-----------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
add_post_type_support( 'post', 'excerpt' );

function get_excerpt($limit, $source = null){
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'... <a href="'.get_permalink($post->ID).'">more</a>';
	return $excerpt;
}

/*-----------------------------------------
  THEME STYLES IN THE VISUAL EDITOR
-----------------------------------------*/
add_editor_style('/dist/styles/main.css');

/*-----------------------------------------
  REMOVE HEADINE ONE FROM WISWIG EDITOR
-----------------------------------------*/
function remove_h1_from_heading($args) {
	$args['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Pre=pre';
	return $args;
}
add_filter('tiny_mce_before_init', 'remove_h1_from_heading' );