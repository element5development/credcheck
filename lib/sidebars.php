<?php

/*-----------------------------------------
		SIDEBARS - www.wp-hasty.com
-----------------------------------------*/
// Register custom sidebars
function utlity_bar() {
	$args = array(
		'name'          => __( 'Utility Bar Left', 'textdomain' ),
		'id'            => 'utility-bar',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'utlity_bar' );
// Register custom footer areas
function footer_areas() {
	$args = array(
		'name'          => __( 'Footer One', 'textdomain' ),
		'id'            => 'footer-one',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Two', 'textdomain' ),
		'id'            => 'footer-two',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Three', 'textdomain' ),
		'id'            => 'footer-three',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Four', 'textdomain' ),
		'id'            => 'footer-four',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_areas' );
// Register custom sidebars
function alert_bar() {
	$args = array(
		'name'          => __( 'Alert Message', 'textdomain' ),
		'id'            => 'alert',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'alert_bar' );