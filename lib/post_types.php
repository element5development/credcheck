<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
function create_cpt() {
// Post Type Key: teammember
	$labels = array(
		'name' => __( 'team', 'Post Type General Name' ),
		'singular_name' => __( 'team member', 'Post Type Singular Name' ),
		'menu_name' => __( 'team' ),
		'name_admin_bar' => __( 'team member' ),
		'archives' => __( 'team member Archives' ),
		'attributes' => __( 'team member Attributes' ),
		'parent_item_colon' => __( 'Parent team member:' ),
		'all_items' => __( 'All team' ),
		'add_new_item' => __( 'Add New team member' ),
		'add_new' => __( 'Add New' ),
		'new_item' => __( 'New team member' ),
		'edit_item' => __( 'Edit team member' ),
		'update_item' => __( 'Update team member' ),
		'view_item' => __( 'View team member' ),
		'view_items' => __( 'View team' ),
		'search_items' => __( 'Search team member' ),
		'not_found' => __( 'Not found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
		'featured_image' => __( 'Featured Image' ),
		'set_featured_image' => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image' => __( 'Use as featured image' ),
		'insert_into_item' => __( 'Insert into team member' ),
		'uploaded_to_this_item' => __( 'Uploaded to this team member' ),
		'items_list' => __( 'team list' ),
		'items_list_navigation' => __( 'team list navigation' ),
		'filter_items_list' => __( 'Filter team list' ),
	);
	$args = array(
		'label' => __( 'team member' ),
		'description' => __( '' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'about/our-team' ),
		'has_archive' => 'about/our-team',
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'teammember', $args );

// Post Type Key: testimony
	$labels = array(
		'name' => __( 'testimonies', 'Post Type General Name' ),
		'singular_name' => __( 'testimony', 'Post Type Singular Name' ),
		'menu_name' => __( 'testimonies' ),
		'name_admin_bar' => __( 'testimony' ),
		'archives' => __( 'testimony Archives' ),
		'attributes' => __( 'testimony Attributes' ),
		'parent_item_colon' => __( 'Parent testimony:' ),
		'all_items' => __( 'All testimonies' ),
		'add_new_item' => __( 'Add New testimony' ),
		'add_new' => __( 'Add New' ),
		'new_item' => __( 'New testimony' ),
		'edit_item' => __( 'Edit testimony' ),
		'update_item' => __( 'Update testimony' ),
		'view_item' => __( 'View testimony' ),
		'view_items' => __( 'View testimonies' ),
		'search_items' => __( 'Search testimony' ),
		'not_found' => __( 'Not found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
		'featured_image' => __( 'Featured Image' ),
		'set_featured_image' => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image' => __( 'Use as featured image' ),
		'insert_into_item' => __( 'Insert into testimony' ),
		'uploaded_to_this_item' => __( 'Uploaded to this testimony' ),
		'items_list' => __( 'testimonies list' ),
		'items_list_navigation' => __( 'testimonies list navigation' ),
		'filter_items_list' => __( 'Filter testimonies list' ),
	);
	$args = array(
		'label' => __( 'testimony' ),
		'description' => __( '' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-editor-quote',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'testimony', $args );
// Post Type Key: resource
	// $labels = array(
	// 	'name' => __( 'resources', 'Post Type General Name' ),
	// 	'singular_name' => __( 'resource', 'Post Type Singular Name' ),
	// 	'menu_name' => __( 'resources' ),
	// 	'name_admin_bar' => __( 'resource' ),
	// 	'archives' => __( 'resource Archives' ),
	// 	'attributes' => __( 'resource Attributes' ),
	// 	'parent_item_colon' => __( 'Parent resource:' ),
	// 	'all_items' => __( 'All resources' ),
	// 	'add_new_item' => __( 'Add New resource' ),
	// 	'add_new' => __( 'Add New' ),
	// 	'new_item' => __( 'New resource' ),
	// 	'edit_item' => __( 'Edit resource' ),
	// 	'update_item' => __( 'Update resource' ),
	// 	'view_item' => __( 'View resource' ),
	// 	'view_items' => __( 'View resources' ),
	// 	'search_items' => __( 'Search resource' ),
	// 	'not_found' => __( 'Not found' ),
	// 	'not_found_in_trash' => __( 'Not found in Trash' ),
	// 	'featured_image' => __( 'Featured Image' ),
	// 	'set_featured_image' => __( 'Set featured image' ),
	// 	'remove_featured_image' => __( 'Remove featured image' ),
	// 	'use_featured_image' => __( 'Use as featured image' ),
	// 	'insert_into_item' => __( 'Insert into resource' ),
	// 	'uploaded_to_this_item' => __( 'Uploaded to this resource' ),
	// 	'items_list' => __( 'resources list' ),
	// 	'items_list_navigation' => __( 'resources list navigation' ),
	// 	'filter_items_list' => __( 'Filter resources list' ),
	// );
	// $args = array(
	// 	'label' => __( 'resource' ),
	// 	'description' => __( '' ),
	// 	'labels' => $labels,
	// 	'menu_icon' => 'dashicons-media-document',
	// 	'supports' => array('title', 'custom-fields', ),
	// 	'taxonomies' => array(),
	// 	'public' => true,
	// 	'show_ui' => true,
	// 	'show_in_menu' => true,
	// 	'menu_position' => 5,
	// 	'show_in_admin_bar' => true,
	// 	'show_in_nav_menus' => true,
	// 	'can_export' => true,
	// 	'rewrite' => array( 'slug' => 'compliance/resource' ),
	// 	'has_archive' => 'compliance/resource',
	// 	'hierarchical' => false,
	// 	'exclude_from_search' => true,
	// 	'show_in_rest' => true,
	// 	'publicly_queryable' => true,
	// 	'capability_type' => 'post',
	// );
	// register_post_type( 'resource', $args );
// Post Type Key: partner
	$labels = array(
		'name' => __( 'partners', 'Post Type General Name' ),
		'singular_name' => __( 'partner', 'Post Type Singular Name' ),
		'menu_name' => __( 'partners' ),
		'name_admin_bar' => __( 'partner' ),
		'archives' => __( 'partner Archives' ),
		'attributes' => __( 'partner Attributes' ),
		'parent_item_colon' => __( 'Parent partner:' ),
		'all_items' => __( 'All partners' ),
		'add_new_item' => __( 'Add New partner' ),
		'add_new' => __( 'Add New' ),
		'new_item' => __( 'New partner' ),
		'edit_item' => __( 'Edit partner' ),
		'update_item' => __( 'Update partner' ),
		'view_item' => __( 'View partner' ),
		'view_items' => __( 'View partners' ),
		'search_items' => __( 'Search partner' ),
		'not_found' => __( 'Not found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
		'featured_image' => __( 'Featured Image' ),
		'set_featured_image' => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image' => __( 'Use as featured image' ),
		'insert_into_item' => __( 'Insert into partner' ),
		'uploaded_to_this_item' => __( 'Uploaded to this partner' ),
		'items_list' => __( 'partners list' ),
		'items_list_navigation' => __( 'partners list navigation' ),
		'filter_items_list' => __( 'Filter partners list' ),
	);
	$args = array(
		'label' => __( 'partner' ),
		'description' => __( '' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-heart',
		'supports' => array('title', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'partner', $args );
// Post Type Key: job
	$labels = array(
		'name' => __( 'jobs', 'Post Type General Name' ),
		'singular_name' => __( 'job', 'Post Type Singular Name' ),
		'menu_name' => __( 'jobs' ),
		'name_admin_bar' => __( 'job' ),
		'archives' => __( 'job Archives' ),
		'attributes' => __( 'job Attributes' ),
		'parent_item_colon' => __( 'Parent job:' ),
		'all_items' => __( 'All jobs' ),
		'add_new_item' => __( 'Add New job' ),
		'add_new' => __( 'Add New' ),
		'new_item' => __( 'New job' ),
		'edit_item' => __( 'Edit job' ),
		'update_item' => __( 'Update job' ),
		'view_item' => __( 'View job' ),
		'view_items' => __( 'View jobs' ),
		'search_items' => __( 'Search job' ),
		'not_found' => __( 'Not found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
		'featured_image' => __( 'Featured Image' ),
		'set_featured_image' => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image' => __( 'Use as featured image' ),
		'insert_into_item' => __( 'Insert into job' ),
		'uploaded_to_this_item' => __( 'Uploaded to this job' ),
		'items_list' => __( 'jobs list' ),
		'items_list_navigation' => __( 'jobs list navigation' ),
		'filter_items_list' => __( 'Filter jobs list' ),
	);
	$args = array(
		'label' => __( 'job' ),
		'description' => __( '' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-id-alt',
		'supports' => array('title', 'editor', 'excerpt', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'careers/job' ),
		'has_archive' => 'careers/job',
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'job', $args );
}
add_action( 'init', 'create_cpt', 0 );


/*-----------------------------------------
	TEAM POST COUNT
-----------------------------------------*/
function set_posts_per_page_for_team( $query ) {
  if ( $query->is_main_query() && is_post_type_archive( 'teammember' ) ) {
    $query->set( 'posts_per_page', '-1' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_team' );

/*-----------------------------------------
	PARTNER POST COUNT
-----------------------------------------*/
function set_posts_per_page_for_partner( $query ) {
  if ( $query->is_main_query() && is_post_type_archive( 'partner' ) ) {
    $query->set( 'posts_per_page', '-1' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_partner' );

/*-----------------------------------------
	PRTNER REDIRECT
-----------------------------------------*/
function partner_single_redirect( $query ) {
  if ( is_singular('partner') ) {
		global $post;
		$redirectLink = get_post_type_archive_link( 'partner' )."#".$post->ID;
		wp_redirect( $redirectLink, 302 );
		exit;
	}
}
add_action( 'pre_get_posts', 'partner_single_redirect' );

/*-----------------------------------------
	RESOURCE POST COUNT
-----------------------------------------*/
// function set_posts_per_page_for_resource( $query ) {
//   if ( $query->is_main_query() && is_post_type_archive( 'resource' ) ) {
//     $query->set( 'posts_per_page', '-1' );
//   }
// }
// add_action( 'pre_get_posts', 'set_posts_per_page_for_resource' );


/*-----------------------------------------
	RESOURCE REDIRECT
-----------------------------------------*/
function resource_single_redirect( $query ) {
  if ( is_singular('resource') ) {
		global $post;
		$redirectLink = get_post_type_archive_link( 'resource' )."#".$post->ID;
		wp_redirect( $redirectLink, 302 );
		exit;
	}
}
add_action( 'pre_get_posts', 'resource_single_redirect' );