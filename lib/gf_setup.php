<?php

/*-----------------------------------------
  CORRECT TAB INDEX ON FORMS
-----------------------------------------*/
add_filter("gform_tabindex", function () {
	return false;
});

/*-----------------------------------------
  EABLE LABEL VISIBILITY OPTION
-----------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*-----------------------------------------
  SUBMIT INPUT TO BUTTON ELEMENT
-----------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button is-secondary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/*-----------------------------------------
  DYNAMIC JOB DROPDOWN
-----------------------------------------*/
add_filter( 'gform_pre_render_6', 'populate_posts' );
add_filter( 'gform_pre_validation_6', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_6', 'populate_posts' );
add_filter( 'gform_admin_pre_render_6', 'populate_posts' );
function populate_posts( $form ) {
	foreach ( $form['fields'] as &$field ) {
			if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
				continue;
			}
			$posts = get_posts( 'post_type=job&numberposts=-1&post_status=publish' );
			$choices = array();
			foreach ( $posts as $post ) {
					$choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
			}
			$field->placeholder = 'Position of Interest';
			$field->choices = $choices;

	}
	return $form;
}