<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation' ),
		'legal_nav' => __( 'Legal Navigation' ),
		'top_nav' => __( 'Top Navigation' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*-----------------------------------------
  SEO Yoast Breadcrumbs
-----------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );
function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);

/*-----------------------------------------
  ACTIVE STATE FOR CHILD ARCHIVES
-----------------------------------------*/
	
function add_current_nav_class($classes, $item) {

	// Getting the current post details
	global $post;

	// Get post ID, if nothing found set to NULL
	$id = ( isset( $post->ID ) ? get_the_ID() : NULL );

	// Checking if post ID exist...
	if (isset( $id )){

			// Getting the post type of the current post
			$current_post_type = get_post_type_object(get_post_type($post->ID));

			// Getting the rewrite slug containing the post type's ancestors
			$ancestor_slug = $current_post_type->rewrite['slug'];

			// Split the slug into an array of ancestors and then slice off the direct parent.
			$ancestors = explode('/',$ancestor_slug);
			$parent = array_pop($ancestors);

			// Getting the URL of the menu item
			$menu_slug = strtolower(trim($item->url));

			// If the menu item URL contains the post type's parent
			if ( $menu_slug && $parent ) {
				if ( strpos($menu_slug,$parent) !== false) {
						$classes[] = 'current-menu-item';
				}
			}

			// If the menu item URL contains any of the post type's ancestors
			foreach ( $ancestors as $ancestor ) {
					if (strpos($menu_slug,$ancestor) !== false) {
							$classes[] = 'current-page-ancestor';
					}
			}
	}
	// Return the corrected set of classes to be added to the menu item
	return $classes;

} add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
