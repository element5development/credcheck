<header class="page-title has-image">
	<div class="block">
		<!-- TITLE -->
		<h1 <?php if ( get_field('title_description') ) { echo 'class="has-subheader"'; } ?>>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
					echo '<span>.</span>';
				else :
					the_title();
					echo '<span>.</span>';
				endif;
			?>
		</h1>
		<!-- /TITLE -->
		<!-- DESCRIPTION -->
		<?php if ( get_field('title_description') ) : ?>
			<p class="subheader">
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>
		<!-- /DESCRIPTION -->
		<!-- BUTTONS -->
		<?php if ( get_field('button') ) : ?>
			<div class="buttons">
				<?php $button = get_field('button'); ?>
				<a class="button is-secondary" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
			</div>
		<?php endif; ?>
		<!-- /BUTTONS -->
	</div>
	<div class="bg-img" style="background-image: url('<?php the_field('title_bg_img'); ?>');">

	</div>
	<!-- BACKGROUND OVERLAY -->
	<div class="overlay"></div>
	<!-- /BACKGROUND OVERLAY -->
	<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pbsa-accredited.png" alt="pbsa accredited" />
	<svg id="golden-triangle" height="443" viewBox="0 0 223 443" width="223" xmlns="http://www.w3.org/2000/svg">
		<linearGradient id="a" x1="7.7911%" x2="50%" y1="15.2016%" y2="100%">
			<stop offset="0" stop-color="#aa8241"/>
			<stop offset="1" stop-color="#c6ae6e"/>
		</linearGradient>
		<g fill="none" fill-rule="evenodd" stroke="url(#a)" stroke-width="6" transform="rotate(-90 219 465)">
			<path d="M68 .0013L461.4987 395 855 0"/>
			<path d="M45 0l416.002 418L877 .0053"/>
			<path d="M22 0l439.5934 441L901 .1873"/>
			<path d="M0 .054L462.4724 464 925 0"/>
		</g>
	</svg>
</header>