<header class="page-title">
	<div class="block">
		<!-- TITLE -->
		<h1 <?php if ( get_field('title_description') ) { echo 'class="has-subheader"'; } ?> >
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
					echo '<span>.</span>';
				else :
					the_title();
					echo '<span>.</span>';
				endif;
			?>
		</h1>
		<!-- /TITLE -->
		<!-- DESCRIPTION -->
		<?php if ( get_field('title_description') ) : ?>
			<p class="subheader">
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>
		<!-- /DESCRIPTION -->
		<!-- BUTTONS -->
		<?php if ( have_rows('title_buttons') ) : $i = 1; ?>
			<div class="buttons">
				<?php while( have_rows('title_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<?php if ( $i == 3 ) :
						$class = 'is-tertiary';
					elseif ( $i == 2 ) :
						$class = 'is-secondary';
					else :
						$class = 'is-primary';
					endif; ?>
					<a class="button <?php echo $class; ?>" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php $i++; endwhile; ?>
			</div>
		<?php endif; ?>
		<!-- /BUTTONS -->
	</div>
</header>