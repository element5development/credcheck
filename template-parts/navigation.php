<div class="utility-block">
	<?php if ( is_active_sidebar( 'utility-bar' ) ) : ?>
			<div class="widget-area">
			<?php dynamic_sidebar( 'utility-bar' ); ?>
			</div>
	<?php endif; ?>
	<div class="nav-block">
		<nav>
			<?php wp_nav_menu(array( 'theme_location' => 'top_nav' )); ?>
		</nav>
		<button ezmodal-target="#appl-login">Login</button>
		<div id="appl-login" class="ezmodal">
			<div class="ezmodal-container">
				<div class="ezmodal-header">
					<div class="ezmodal-close" data-dismiss="ezmodal">x</div>
				</div>
				<div class="ezmodal-content">
					<h2>Welcome Back!</h2>
					<p>CredentialCheck APPOL 2.0</p>
					<div class="block">
						<form action="https://www.credentialcheck.net/csge/externallogin.aspx" method="post" name="dataform1" id="dataform1" class="signup-form" target="_blank" novalidate="novalidate">
							<div class="gform_body">
								<ul class="gform_fields top_label form_sublabel_below description_below">
									<li class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
										<label class="gfield_label" for="loginname">Username</label>
										<div class="ginput_container ginput_container_text">
											<input class="inputs valid" id="loginname" name="loginname">
										</div>
									</li>
									<li class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
										<label class="gfield_label" for="password">Password</label>
										<div class="ginput_container ginput_container_text">
											<input class="inputs valid" id="password" name="password" type="password">
										</div>
									</li>
									<li class="gfield is-hidden field_sublabel_below field_description_below gfield_visibility_visible">
										<label class="gfield_label" for="password">Password</label>
										<div class="ginput_container ginput_container_text">
											<input id="hmnchk1" name="hmnchk1" type="hidden" value="http://">
											<input id="hmnchk2" name="hmnchk2" type="hidden" value="">
											<input id="page" name="page" type="hidden" value="2">
										</div>
									</li>
								</ul>
							</div>
							<div class="gform_footer top_label"> 
								<button type="submit" class="button is-secondary">Login</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="navigation-block">
	<a href="<?php echo get_home_url(); ?>">
		<svg>
			<use xlink:href="#logo" />
		</svg>
	</a>
	<div class="hamburger-menu"></div>
	<nav>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
		<div class="mobile-nav">
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'top_nav' )); ?>
			</nav>
			<button id="modal-menu-close" ezmodal-target="#appl-login">Login</button>
		</div>
		<button id="search-button">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<div id="search-form">
			<?php get_search_form(); ?>
			<button id="search-close">
				<span>Cancel</span>
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
		</div>
		<div class="close"></div>
	</nav>
</div>