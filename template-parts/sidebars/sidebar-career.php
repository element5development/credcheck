<div class="contents">

	<h3>Want to join our amazing team?</h3>

	<h4>Current Openings</h4>

	<!-- JOBS QUERY -->
	<?php
		$args = array(
		'post_type'        => 'job',
		'posts_per_page'   => -1,
		);
		$query = new WP_Query( $args ); 
	?>
	<?php if ( $query->have_posts() ) { ?>
		<?php while ( $query->have_posts() ) { $query->the_post();  ?> 
			<a href="<?php the_permalink(); ?>" class="job-preview">
				<h5><?php the_title(); ?></h5>
				<p><?php the_excerpt(); ?></p>
			</a>
		<?php } ?>
	<?php } else { ?>
		<p>We don’t currently have any openings, but we are always looking for new team members. Send us your resume today!</p>
	<?php } wp_reset_query(); ?>

	<h4>Apply Now</h4>

	<?php echo do_shortcode('[gravityform id="6" title="false" description="false"]'); ?>

</div>