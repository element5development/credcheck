<section class="testimony-glide">
	<h2>What our clients say about us</h2>
	<div class="glide">
		
				<?php
					$args = array(
					'post_type'        => 'testimony',
					'posts_per_page'   => -1,
					);
					$query = new WP_Query( $args ); 
				?>
				<?php if ( $query->have_posts() ) { ?>
					<?php while ( $query->have_posts() ) { $query->the_post();  ?> 
						<div class="glide__slide">
							<p class="has-icon">
								<?php $icon = get_field('icon'); ?>
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
								<?php the_field('industry'); ?>
							</p>
							<p><?php the_field('quote'); ?></p>
						</div>
					<?php } ?>
				<?php } wp_reset_query(); ?>


	</div>
</section>