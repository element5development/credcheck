<article class="preview-member">
	<?php $headshot = get_field('headshot'); ?>
	<a href="<?php the_permalink(); ?>">
		<div class="headshot-contain" style="background-image: url(<?php echo $headshot['url']; ?>);">
		</div>
		<h2><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h2>
		<p><?php the_field('job_title'); ?></p>
	</a>
	<div class="buttons">
		<?php if ( get_field('phone') ) : ?>
			<?php 
				$phone = preg_replace('/[^0-9]/', '', get_field('phone'));
			?>
			<a class="button is-icon is-ghost is-borderless is-small" href="tel:+1<?php echo $phone; ?>">
				<svg>
					<use xlink:href="#phone" />
				</svg>
			</a>
		<?php endif; ?>
		<?php if ( get_field('email') ) : ?>
			<a target="_blank" class="button is-icon is-ghost is-borderless is-small" href="mailto:<?php the_field('email'); ?>">
				<svg>
					<use xlink:href="#email" />
				</svg>
			</a>
		<?php endif; ?>
		<?php if ( get_field('linkedin') ) : ?>
			<a target="_blank" class="button is-icon is-ghost is-borderless is-small" href="<?php the_field('linkedin'); ?>">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
		<?php endif; ?>
	</div>
</article>