<div class="icon-cards">
	<h2><?php the_field('icon_cards_headline'); ?></h2>
	<?php while( have_rows('icon_cards') ) : the_row(); ?>
		<div class="icon-card">
			<?php $icon = get_sub_field('icon'); ?>
			<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
			<h3><?php the_sub_field('headline'); ?></php></h3>
			<p><?php the_sub_field('description'); ?></p>
		</div>
	<?php endwhile; ?>
</div>