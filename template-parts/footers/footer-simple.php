<footer class="page-footer">
	<div class="copyright">
		<div class="grid has-two-column">
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
		</div>
	</div>
</footer>