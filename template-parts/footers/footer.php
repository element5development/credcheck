<footer class="page-footer">
	<svg id="bottom-lines" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 210 210">
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="70.79" y1="140.3952" x2="210" y2="140.3952">
			<stop offset=".0639" stop-color="#bf9c51"/>
			<stop offset="1" stop-color="#af8338"/>
			<stop offset="1" stop-color="#af8237"/>
		</linearGradient>
		<path fill="url(#SVGID_1_)" d="M210 70.8v8.5L79.3 210h-8.5z"/>
		<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="47.88" y1="128.9402" x2="210" y2="128.9402">
			<stop offset=".0639" stop-color="#bf9c51"/>
			<stop offset="1" stop-color="#af8338"/>
			<stop offset="1" stop-color="#af8237"/>
		</linearGradient>
		<path fill="url(#SVGID_2_)" d="M210 47.9v8.5L56.4 210h-8.5z"/>
		<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="24.98" y1="117.4852" x2="210" y2="117.4852">
			<stop offset=".0639" stop-color="#bf9c51"/>
			<stop offset="1" stop-color="#af8338"/>
			<stop offset="1" stop-color="#af8237"/>
		</linearGradient>
		<path fill="url(#SVGID_3_)" d="M210 25v8.5L33.5 210H25z"/>
		<linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="2.07" y1="106.0351" x2="210" y2="106.0351">
			<stop offset=".0639" stop-color="#bf9c51"/>
			<stop offset="1" stop-color="#af8338"/>
			<stop offset="1" stop-color="#af8237"/>
		</linearGradient>
		<path fill="url(#SVGID_4_)" d="M210 2.1v8.5L10.6 210H2.1z"/>
	</svg>
	<div class="blocks">
		<div class="block">
			<?php dynamic_sidebar('footer-one'); ?>
		</div>
		<div class="block">
			<?php dynamic_sidebar('footer-two'); ?>
		</div>
		<div class="block">
			<?php dynamic_sidebar('footer-three'); ?>
		</div>
		<div class="block">
			<?php dynamic_sidebar('footer-four'); ?>
		</div>
	</div>
	<div id="element5-credit">
		<a target="_blank" href="https://element5digital.com">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
	<div class="copyright">
		<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		<nav>
			<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			<button class="cookie-open">Cookie Consent</button>
		</nav>
	</div>
</footer>