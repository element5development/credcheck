<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<!DOCTYPE html>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<title>
		<?php wp_title(''); ?>
	</title>
	<?php wp_head(); ?>
	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager head code here
		| Hot Jar tracking will be added within Google Tag Manager
		|
		\*----------------------------------------------------------------*/
		?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KP4VNGS');</script>
		<!-- End Google Tag Manager -->
	<?php } ?>
	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"marketing\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop any marketing tracking pixels here
		| These are related to paid advertising like Google Ads
		| and Facebook ad pixels.
		|
		\*----------------------------------------------------------------*/
		?>
	<?php } ?>
</head>

<body <?php body_class(); ?>>

	<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager body code here
		|
		\*----------------------------------------------------------------*/
		?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KP4VNGS"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<?php } ?>

	<?php get_template_part('template-parts/icon-set'); ?>