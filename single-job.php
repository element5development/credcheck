<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">
	<!-- PAGE TITLE -->
	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>
	<!-- /PAGE TITLE -->
	<!-- PAGE CONTENT -->
	<main>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<div class="main-content-block">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
		</article>
		<?php if ( comments_open() || get_comments_number() ) : ?>
			<?php comments_template(); ?>
		<?php endif; ?>
	</main>
	<!-- /PAGE CONTENT -->
	<!-- PAGE FOOTER -->
	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
	<!-- /PAGE FOOTER -->
</div>

<?php get_footer(); ?>