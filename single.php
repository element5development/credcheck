<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-full-width">
	<header class="page-title">
		<div class="block">
			<h1 <?php if ( get_field('title_description') ) { echo 'class="has-subheader"'; } ?> >
				<?php 
					if ( get_field('page_title') ) :
						the_field('page_title');
						echo '<span>.</span>';
					else :
						the_title();
						echo '<span>.</span>';
					endif;
				?>
			</h1>
			<?php if ( get_field('title_description') ) : ?>
				<p class="subheader">
					<?php the_field('title_description'); ?>
				</p>
			<?php endif; ?>
		</div>
	</header>

	<main id="fixed-container">
		<?php if ( get_field('title_bg_img') ) : ?>
			<?php $image = get_field('title_bg_img'); ?>
			<img class="featured-image" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<div class="main-content-block">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
			<?php get_template_part('template-parts/elements/share'); ?>
		</article>
		<?php get_template_part('template-parts/elements/related'); ?>
		<?php if ( comments_open() || get_comments_number() ) : ?>
			<?php comments_template(); ?>
		<?php endif; ?>
	</main>
	<!-- /PAGE CONTENT -->
	<!-- PAGE FOOTER -->
	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
	<!-- /PAGE FOOTER -->
</div>

<?php get_footer(); ?>