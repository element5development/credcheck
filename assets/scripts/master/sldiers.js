var $ = jQuery;

jQuery(document).ready(function () {
	/*----------------------------------------------------------------*\
			ICON SLIDERS
	\*----------------------------------------------------------------*/
	$('.glide').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
});