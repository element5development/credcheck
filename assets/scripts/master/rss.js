var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			LIMIT TITLES
	\*----------------------------------------------------------------*/
	$(".feed-item-title a").each(function (i) {
		var len = $(this).text().length;
		if (len > 50) {
			$(this).text($(this).text().substr(0, 50) + '...');
		}
	});

});