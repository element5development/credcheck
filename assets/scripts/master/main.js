var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		COOKIE CONSENT
		Note: Accepted tracking cookies should be enabled in Google Tag Manager
		https://www.grahamomaonaigh.com/gdpr-compliant-cookies-google-analytics/
	\*----------------------------------------------------------------*/
	// Available options located at https://github.com/ketanmistry/ihavecookies
	var options = {
		title: 'Cookies & Privacy',
		message: 'Cookie Policy - We use cookies to ensure that we give you the best possible experience on our website. By using this website, you agree to the use of cookies. To learn more, please read our <a href="/privacy-policy/">Privacy Policy</a>.',
		link: '/privacy-policy/',
		delay: 0,
		expires: 30, // 30 days
		moreInfoLabel: '',
		acceptBtnLabel: 'I Agree',
		advancedBtnLabel: 'Customize',
		cookieTypesTitle: 'Select cookies to accept',
		uncheckBoxes: false,
		// Optional callback function when 'Accept' button is clicked
		onAccept: function () {
			var myPreferences = $.fn.ihavecookies.cookie();
			console.log('The following cookie preferences have been saved:');
			console.log(myPreferences);
		},
		// Array of cookie types for which to show checkboxes.
		// - type: Type of cookie. This is also the label that is displayed.
		// - value: Value of the checkbox so it can be easily identified in your application.
		// - description: Description for this cookie type. Displayed in title attribute.
		cookieTypes: [{
				type: 'Analytics',
				value: 'analytics',
				description: 'Cookies related to site visits, browser types, etc.'
			},
			{
				type: 'Marketing',
				value: 'marketing',
				description: 'Cookies related to marketing, e.g. newsletters, social media, etc'
			}
		],
	}
	$('body').ihavecookies(options);
	// Reopen cookie consent
	$('button.cookie-open').click(function () {
		$('body').ihavecookies(options, 'reinit');
	});
	// Check if preference is selected
	if ($.fn.ihavecookies.preference('analytics') === true) {
		console.log('Analytics cookies have been accepted.');
	}
	if ($.fn.ihavecookies.preference('marketing') === true) {
		console.log('Marketing cookies have been accepted.');
	}
	/*------------------------------------------------------------------
  	ENTRANCE ANIMATIONS
	------------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*------------------------------------------------------------------
		INPUT ADDING AND REMVOING CLASSES
	------------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').focus(function () {
		$(this).addClass('is-activated');
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	});
	/*------------------------------------------------------------------
  	LOGIC FOR LOAD MORE BUTTON
	------------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*------------------------------------------------------------------
		INFINITE SCROLL INIT
	------------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview-post',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
	});
	$('.feed-search').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview-search',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
	});
	/*------------------------------------------------------------------
		SEARCH FORM
	------------------------------------------------------------------*/
	$("#search-button").click(function () {
		$("#search-form").addClass("is-active");
	});
	$("#search-close").click(function () {
		$("#search-form").removeClass("is-active");
	});
	/*------------------------------------------------------------------
		FORM LABELS
	------------------------------------------------------------------*/
	$('input').focus(function () {
		$(this).closest('li').addClass('is-active');
	});
	$('input').blur(function () {
		tmpval = $(this).val().length;
		$(this).closest('li').removeClass('is-active');
		if (tmpval > 0) {
			$(this).closest('li').addClass('has-value');
		} else {
			$(this).closest('li').removeClass('has-value');
		}
	});
	$('textarea').focus(function () {
		$(this).closest('li').addClass('is-active');
	});
	$('textarea').blur(function () {
		taval = $(this).val().length;
		$(this).closest('li').removeClass('is-active');
		if (taval > 0) {
			$(this).closest('li').addClass('has-value');
		} else {
			$(this).closest('li').removeClass('has-value');
		}
	});
	/*------------------------------------------------------------------
		HAMBURGER
	------------------------------------------------------------------*/
	$(".hamburger-menu").click(function () {
		$(this).toggleClass("is-active");
		$('.navigation-block nav').toggleClass("is-active");
	});
	$(".navigation-block nav .close").on('click', function () {
		$(".hamburger-menu").removeClass("is-active");
		$('.navigation-block nav').removeClass("is-active");
	});
	/*------------------------------------------------------------------
		FILE UPLOAD
	------------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*------------------------------------------------------------------
		MOBILE MODAL
	------------------------------------------------------------------*/
	$("#modal-menu-close").click(function () {
		$(".hamburger-menu").removeClass("is-active");
		$('.navigation-block nav').removeClass("is-active");
	});
	/*------------------------------------------------------------------
		STICKY BAR
	------------------------------------------------------------------*/
	$('#social-share-fixed').stickySidebar({
		topSpacing: 175,
		bottomSpacing: 175
	});

});