<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<?php 
	$team = $_GET['team'];
	if ( !$team ) :
		$team = 'team';
	endif;
?>

<header class="page-title has-image" style="background-image: url('<?php the_field( $team . '_title_bg_img', 'option'); ?>');">
	<div class="block">
		<h1 class="has-subheader"><?php the_field( $team . '_page_title', 'option'); ?><span>.</span></h1>
		<p class="subheader"><?php the_field( $team . '_title_description', 'option'); ?></p>
	</div>
	<!-- BACKGROUND OVERLAY -->
	<div class="overlay"></div>
	<!-- /BACKGROUND OVERLAY -->
	<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pbsa-accredited.png" alt="pbsa accredited" />
	<svg id="golden-triangle" height="443" viewBox="0 0 223 443" width="223" xmlns="http://www.w3.org/2000/svg">
		<linearGradient id="a" x1="7.7911%" x2="50%" y1="15.2016%" y2="100%">
			<stop offset="0" stop-color="#aa8241"/>
			<stop offset="1" stop-color="#c6ae6e"/>
		</linearGradient>
		<g fill="none" fill-rule="evenodd" stroke="url(#a)" stroke-width="6" transform="rotate(-90 219 465)">
			<path d="M68 .0013L461.4987 395 855 0"/>
			<path d="M45 0l416.002 418L877 .0053"/>
			<path d="M22 0l439.5934 441L901 .1873"/>
			<path d="M0 .054L462.4724 464 925 0"/>
		</g>
	</svg>
</header>

<main>

	<?php
			$exclude = [];
			foreach( get_pages(['child_of' => 253, 'parent' => 253,'meta_key' => '_wp_page_template', 'meta_value' => 'templates/thankyou.php']) as $page) {
					$exclude[] = $page->post_id;
			}
			$childpages = get_pages( array( 
				'child_of' => 253, 
				'parent' => 253,
				'exclude'      => implode(",", $exclude),
				'hierarchical' => 0,
				'sort_column' => 'menu_order', 
				'sort_order' => 'asc',
			) );
		?>
		<nav class="page-nav">
			<a class="button is-ghost" href="<?php the_permalink(253); ?>"><?php echo get_the_title(253); ?></a>
			<?php if ( $team == 'board' ) : ?>
				<a class="button is-ghost" href="/about/our-team/?team=team">Our Team</a>
				<!-- <a class="button is-ghost is-active" href="/about/our-team/?team=board">Our Board</a> -->
			<?php else : ?>
				<a class="button is-ghost is-active" href="/about/our-team/?team=team">Our Team</a>
				<!-- <a class="button is-ghost" href="/about/our-team/?team=board">Our Board</a> -->
			<?php endif; ?>
			<?php foreach( $childpages as $page ) { ?>
				<a class="button is-ghost" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a>
			<?php } ?>
		</nav>


	<a id="content" class="anchor"></a>
	<?php if ( get_field( $team . '_editor', 'option') ) : ?>
		<article class="main-content-block ">
			<?php the_field( $team . '_editor', 'option'); ?>
		</article>
	<?php endif; ?>
	<section class="feed default-contents	">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php if ( $team == get_field('team_or_board') ) : 
				get_template_part( 'template-parts/elements/preview-member' ); 
			else : 
				//skip this post
			endif; 
			?>
		<?php endwhile; ?>
	</section>

	<section class="infinite-scroll">
		<?php the_posts_pagination( array(
			'prev_text'	=> __( 'Previous page' ),
			'next_text'	=> __( 'Next page' ),
		) ); ?>
		<a class="button is-primary load-more">View more</a>
	</section>
</main>

<?php get_template_part('template-parts/footers/footer'); ?>

<?php get_footer(); ?>