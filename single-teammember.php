<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="is-sidebar-left">
	<header class="page-title has-image" style="background-image: url('/wp-content/uploads/2018/05/CC-Header-About.jpg');">
		<div class="block">
			<h1 class="has-subheader"><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h1>
			<p class="subheader"><?php the_field('job_title'); ?></p>
		</div>
		<!-- BACKGROUND OVERLAY -->
		<div class="overlay"></div>
		<!-- /BACKGROUND OVERLAY -->
		<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pbsa-accredited.png" alt="pbsa accredited" />
		<svg id="golden-triangle" height="443" viewBox="0 0 223 443" width="223" xmlns="http://www.w3.org/2000/svg">
			<linearGradient id="a" x1="7.7911%" x2="50%" y1="15.2016%" y2="100%">
				<stop offset="0" stop-color="#aa8241"/>
				<stop offset="1" stop-color="#c6ae6e"/>
			</linearGradient>
			<g fill="none" fill-rule="evenodd" stroke="url(#a)" stroke-width="6" transform="rotate(-90 219 465)">
				<path d="M68 .0013L461.4987 395 855 0"/>
				<path d="M45 0l416.002 418L877 .0053"/>
				<path d="M22 0l439.5934 441L901 .1873"/>
				<path d="M0 .054L462.4724 464 925 0"/>
			</g>
		</svg>
	</header>
	<?php
		$exclude = [];
		foreach( get_pages(['child_of' => 253, 'parent' => 253,'meta_key' => '_wp_page_template', 'meta_value' => 'templates/thankyou.php']) as $page) {
				$exclude[] = $page->post_id;
		}
		$childpages = get_pages( array( 
			'child_of' => 253, 
			'parent' => 253,
			'exclude'      => implode(",", $exclude),
			'hierarchical' => 0,
			'sort_column' => 'menu_order', 
			'sort_order' => 'asc',
		) );
	?>
	<nav class="page-nav">
		<a class="button is-ghost" href="<?php the_permalink(253); ?>"><?php echo get_the_title(253); ?></a>
		<?php if ( $team == 'board' ) : ?>
			<a class="button is-ghost" href="/about/our-team/?team=team">Our Team</a>
			<!-- <a class="button is-ghost is-active" href="/about/our-team/?team=board">Our Board</a> -->
		<?php else : ?>
			<a class="button is-ghost is-active" href="/about/our-team/?team=team">Our Team</a>
			<!-- <a class="button is-ghost" href="/about/our-team/?team=board">Our Board</a> -->
		<?php endif; ?>
		<?php foreach( $childpages as $page ) { ?>
			<a class="button is-ghost" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a>
		<?php } ?>
	</nav>
	<div class="ie-block">
		<main>
			<a id="content" class="anchor"></a>
			<article>
				<?php if ( '' !== get_post()->post_content ) : ?>
					<div class="main-content-block">
						<?php the_content(); ?>
					</div>
				<?php endif; ?>
			</article>
		</main>
		<aside>
			<?php $headshot = get_field('headshot'); ?>
			<img src="<?php echo $headshot['url']; ?>" alt="<?php echo $headshot['alt']; ?>" />
			<div class="buttons">
				<?php if ( get_field('phone') ) : ?>
					<?php 
						$phone = preg_replace('/[^0-9]/', '', get_field('phone'));
					?>
					<a class="button is-icon is-ghost is-borderless is-small" href="tel:+1<?php echo $phone; ?>">
						<svg>
							<use xlink:href="#phone" />
						</svg>
					</a>
				<?php endif; ?>
				<?php if ( get_field('email') ) : ?>
					<a target="_blank" class="button is-icon is-ghost is-borderless is-small" href="mailto:<?php the_field('email'); ?>">
						<svg>
							<use xlink:href="#email" />
						</svg>
					</a>
				<?php endif; ?>
				<?php if ( get_field('linkedin') ) : ?>
					<a target="_blank" class="button is-icon is-ghost is-borderless is-small" href="<?php the_field('linkedin'); ?>">
						<svg>
							<use xlink:href="#linkedin" />
						</svg>
					</a>
				<?php endif; ?>
			</div>
			<?php if( have_rows('certifications') ): ?>
				<div class="certifications">
					<?php while ( have_rows('certifications') ) : the_row(); ?>
						<div class="certification">
							<?php $icon = get_sub_field('icon'); ?>
							<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</aside>
	</div>
	<nav class="post-nav">
		<?php 
			$current_type = get_field('team_or_board');
			$prev_post = get_previous_post(); 
			$prev_type = get_field('team_or_board', $prev_post->ID);
			$next_post = get_next_post(); 
			$next_type = get_field('team_or_board', $next_post->ID);
		?>
		<?php if (	$current_type == $next_type	) : ?>
			<div class="next">
					<?php next_post_link('<svg><use xlink:href="#arrowhead-left" /></svg><strong>%link</strong>'); ?>
			</div>
		<?php endif ?>
		<?php if (	$current_type == $prev_type	) : ?>
			<div class="prev">
				<?php previous_post_link('<strong>%link</strong><svg><use xlink:href="#arrowhead-right" /></svg>'); ?>
			</div>
		<?php endif ?>
	</nav>
	<?php get_template_part('template-parts/footers/footer'); ?>
</div>

<?php get_footer(); ?>