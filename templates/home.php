<?php 
/*-------------------------------------------------------------------
    Template Name: Home
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-home">
	
	<?php get_template_part('template-parts/headers/header-home'); ?>

	<main role="main">
		<a id="content" class="anchor"></a>
		<section class="company-size-cta">
			<h2>Choose your company size to get started</h2>
			<div class="grid has-two-column">
				<a class="employees" href="<?php the_permalink(401); ?>">
					<svg>
						<use xlink:href="#under-500" />
					</svg>
					<p>Our company has:</p>
					<h3>Under 500 Employees</h3>
					<div class="button is-secondary">Let's Do This</div>
				</a>
				<a class="employees" href="<?php the_permalink(403); ?>">
					<svg>
						<use xlink:href="#over-500" />
					</svg>
					<p>Our company has:</p>
					<h3>Over 500 Employees</h3>
					<div class="button is-secondary">Let's Get Started</div>
				</a>
			</div>
			<span>OR</span>
		</section>
		<section class="basic-cta">
			<h2>We’re everything you’re looking for</h2>
			<a class="button is-primary" href="<?php the_permalink(258); ?>">The Services We Offer</a>
			<a class="button is-primary" href="<?php echo get_post_type_archive_link('partner'); ?>">Who We Partner With</a>
		</section>
		<article class="grid has-three-column">
			<div class="col">
				<?php the_field('col_one'); ?>
			</div>
			<div class="col">
				<?php the_field('col_two'); ?>
			</div>
			<div class="col">
				<?php the_field('col_three'); ?>
			</div>
			<div class="cta">
				<a class="button is-secondary" href="<?php the_permalink(266); ?>">Talk to Us</a>
			</div>
		</article>
		<section class="industries grid has-five-column">
			<h2>Industries we specialize in:</h2>
			<a class="industry" href="<?php the_permalink(382); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Icons_Finance.svg" alt="Finance" />
				<h3>Finance</h3>
				<div class="button is-primary">Learn More</div>
			</a>
			<a class="industry" href="<?php the_permalink(383); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Icons_Manufacturing.svg" alt="Manufacturing" />
				<h3>Manufacturing</h3>
				<div class="button is-primary">Learn More</div>
			</a>
			<a class="industry" href="<?php the_permalink(384); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Icons_Food&Retail.svg" alt="Food & Retail" />
				<h3>Food & Retail</h3>
				<div class="button is-primary">Learn More</div>
			</a>
			<a class="industry" href="<?php the_permalink(385); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Icons_Healthcare.svg" alt="Healthcare" />
				<h3>Healthcare</h3>
				<div class="button is-primary">Learn More</div>
			</a>
			<a class="industry" href="<?php the_permalink(386); ?>">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Icons_Staffing.svg" alt="Staffing" />
				<h3>Staffing</h3>
				<div class="button is-primary">Learn More</div>
			</a>
		</section>
		
		<?php if ( have_rows('awards') ) : ?>
			<section class="awards">
				<h2>Awards & Recognitions</h2>
				<?php while( have_rows('awards') ) : the_row(); ?>
					<?php 
						$image = get_sub_field('award'); 
						$award = $image['sizes']['small'];
					?>
					<?php if ( get_sub_field('link') ) : ?>
					<a class="award" href="<?php the_sub_field('link'); ?>" target="_blank">
						<img src="<?php echo $award; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
					<?php else : ?>
					<div class="award">
						<img src="<?php echo $award; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<?php endif; ?>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
	</main>
	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
</div>

<?php get_footer(); ?>