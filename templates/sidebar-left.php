<?php 
/*-------------------------------------------------------------------
		Template Name: Sidebar Left
-------------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="page-block is-sidebar-left">
	<!-- PAGE TITLE -->
	<?php
		if ( get_field('title_bg_vid') ) :
			get_template_part('template-parts/headers/header-video');
		elseif ( get_field('title_bg_img') ) :
			get_template_part('template-parts/headers/header-image');
		else :
			get_template_part('template-parts/headers/header');
		endif;
	?>
	<!-- /PAGE TITLE -->
	<!-- PAGE CONTENT -->
	<main>
	<?php
			$exclude = [];
			foreach( get_pages(['child_of' => $post->ID, 'parent' => $post->ID,'meta_key' => '_wp_page_template', 'meta_value' => 'templates/thankyou.php']) as $page) {
					$exclude[] = $page->post_id;
			}
			$childpages = get_pages( array( 
				'child_of' => $post->ID, 
				'parent' => $post->ID,
				'exclude'      => implode(",", $exclude),
				'hierarchical' => 0,
				'sort_column' => 'menu_order', 
				'sort_order' => 'asc',
			) );
			if ( !$childpages && wp_get_post_parent_id($post->ID) != 0 ) :
				$parentpages = get_pages( array( 
					'child_of' => wp_get_post_parent_id($post->ID), 
					'parent' => wp_get_post_parent_id($post->ID),
					'exclude'      => implode(",", $exclude),
					'hierarchical' => 0,
					'sort_column' => 'menu_order', 
					'sort_order' => 'asc',
				) );
			endif;
		?>
		<?php if ( $childpages || is_page('253') ) : ?>
			<nav class="page-nav">
				<a class="button is-ghost is-active" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php if ( is_page('253') ) : ?>
					<a class="button is-ghost" href="/about/our-team/">Our Team</a>
					<!-- <a class="button is-ghost" href="/about/our-team/?team=board">Our Board</a> -->
				<?php endif; ?>
				<?php foreach( $childpages as $page ) { ?>
					<a class="button is-ghost" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a>
				<?php } ?>
			</nav>
		<?php endif; ?>
		<?php if ( $parentpages  ) : ?>
			<nav class="page-nav">
				<a class="button is-ghost" href="<?php the_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>
				<?php if ( is_page('253') || wp_get_post_parent_id($post->ID) == '253' ) : ?>
					<a class="button is-ghost" href="/about/our-team/?team=team">Our Team</a>
					<!-- <a class="button is-ghost" href="/about/our-team/?team=board">Our Board</a> -->
				<?php endif; ?>
				<?php foreach( $parentpages  as $page ) { ?>
					<a class="button is-ghost <?php if ( $post->ID == $page->ID ) { echo 'is-active'; } ?>" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a>
				<?php } ?>
			</nav>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		<a id="content" class="anchor"></a>
		<article>
			<?php if ( '' !== get_post()->post_content ) : ?>
				<div class="main-content-block">
					<?php the_content(); ?>
					<?php 
						if ( have_rows('icon_cards') ) : 
							get_template_part('template-parts/elements/preview-icon-card');
						endif; 
					?>
				</div>
			<?php endif; ?>
		</article>
	</main>
	<!-- /PAGE CONTENT -->
	<!-- PAGE SIDEBAR -->
	<aside>
		<?php 
			if ( get_field('content_type') == 'jobs' ) :
				get_template_part('template-parts/sidebars/sidebar-career');
			elseif ( get_field('content_type') == 'rss' ) :
				get_template_part('template-parts/sidebars/sidebar-rss');
			else :
				get_template_part('template-parts/sidebars/sidebar-blog');
			endif;
		 ?>
	</aside>
	<!-- /PAGE SIDEBAR -->
	<!-- PAGE FOOTER -->
	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
	<!-- /PAGE FOOTER -->
</div>

<?php get_footer(); ?>