<?php 
/*-------------------------------------------------------------------
    Template Name: Form
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/navigation'); ?>

<div class="is-form-page">
	<!-- PAGE TITLE -->
	<?php	get_template_part('template-parts/headers/header'); ?>
	<!-- /PAGE TITLE -->
	<!-- PAGE CONTENT -->
	<div class="ie-block">
		<main>
			<a id="content" class="anchor"></a>
			<article>
				<?php if ( '' !== get_post()->post_content ) : ?>
					<div class="main-content-block">
						<?php the_content(); ?>
					</div>
				<?php endif; ?>
			</article>
		</main>
		<!-- /PAGE CONTENT -->
		<!-- PAGE SIDEBAR -->
		<aside>
			<?php get_template_part('template-parts/sidebars/sidebar-editor'); ?>
		</aside>
	</div>
	<!-- /PAGE SIDEBAR -->
	<!-- PAGE FOOTER -->
	<?php 
		if ( get_field('footer_style') == 'simple' ) : 
			get_template_part('template-parts/footers/footer-simple');
		else : 
			get_template_part('template-parts/footers/footer');
		endif; 
	?>
	<!-- /PAGE FOOTER -->
</div>

<?php get_footer(); ?>