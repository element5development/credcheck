<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php wp_footer(); ?>

<!-- START OF LIVECHAT CODE -->
<script type="text/javascript">
	setTimeout(function() {
	window.__lc = window.__lc || {};
	window.__lc.license = 10383342;
	(function() {
	var lc = document.createElement('script');
	lc.type = 'text/javascript';
	lc.async = true;
	lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(lc, s);
	})();
	}, 500);
</script>
<!-- END OF LIVE CHAT CODE -->

</body>

</html>